<?php

namespace Drupal\iwfm_committee_contact\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\xai\Form
 */
class ContactCommitteeConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'committee.email.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'committee_email_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('committee.email.settings');
    $form['send_email_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Committee Send Email (API URL)'),
      '#default_value' => $config->get('send_email_path'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('committee.email.settings')
      ->set('send_email_path', $form_state->getValue('send_email_path'))
      ->save();
  }

}

<?php

namespace Drupal\iwfm_committee_contact\Form;

use Drupal\user\Entity\User;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;

/**
 * Contact your Committee.
 */
class ContactCommitteeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contact_your_committee';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Initialise default values.
    $contactid = NULL;
    $api_url_contactid = NULL;
    $response_contactid = NULL;
    $api_url_committee = NULL;
    $response_committee = [];
    $community_data = [];
    $community_group_options = [];

    // Get config data.
    $config = \Drupal::config('bifm.api_interface');

    // Get environment.
    // @todo - Function definition is wrong and it should have correct module
    //   name as prefix i.e bifm_api_interface_.
    $environment = bifm_get_environment();

    // Get subscription key.
    // @todo - Function definition is wrong and it should have correct module
    //   name as prefix i.e bifm_api_interface_.
    $subscription_key = bifm_get_subscription_key();

    // Load current user object.
    $user = User::load(\Drupal::currentUser()->id());

    // Get config value for this API call.
    $api_url_contactid = $config->get($environment . '_get_contact_from_email');

    // Replace {email} with the current logged in user's email address and
    // remove any white spaces.
    $api_url_contactid = trim(str_replace('{email}',
     $user->get('mail')->value, $api_url_contactid));

    // Make an API call to get contactid.
    $response_contactid = iwfm_committee_contact_api_connect(
      $subscription_key, $api_url_contactid);

    // Process this block if API has returned a result.
    if ($response_contactid) {
      // Decode JSON data and assign contactid value.
      $contactid = json_decode($response_contactid)->contactid;
    }

    // Community Group.
    // Get config value for this API call.
    $api_url_committee = $config->get($environment . '_get_committee_iam_partof');

    // Replace {contactid} with $contactid and remove any white spaces.
    $api_url_committee = trim(str_replace('{contactid}',
      $contactid, $api_url_committee));

    // Make an API call to get Community Group data.
    $response_committee = iwfm_committee_contact_api_connect(
      $subscription_key, $api_url_committee);

    // Process this block if API has returned a result.
    if ($response_committee) {
      // Decode JSON data.
      $community_data = json_decode($response_committee);
    }

    // Loop through the AZURE API result data and prepare data for the drop
    // down list.
    foreach ($community_data as $value_rc) {
      $community_group_options[$value_rc->CommunityGroupId] =
      $value_rc->CommunityGroupName;
    }

    // Process this block if API has returned Community Group.
    if (count(json_decode($response_committee)) > 0) {
      // Define form fields.
      $form['form_heading_1'] = [
        '#markup' => $this->t('<h2>Contact your Committee</h2>'),
      ];

      $form['form_heading_2'] = [
        '#markup' => $this->t('<b>Form Details (* = mandatory fields):</b>'),
      ];

      $form['contact_id'] = [
        '#type' => 'hidden',
        '#default_value' => $contactid,
      ];

      $form['community_group'] = [
        '#type' => 'select',
        '#title' => $this->t('Community Group'),
        '#options' => $community_group_options,
        '#required' => TRUE,
      ];

      $form['email_subject'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Email Subject'),
        '#required' => TRUE,
      ];

      $form['email_message'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Email Message'),
        '#required' => TRUE,
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => 'Send Email',
      ];

      return $form;
    }

    // Display error message if API has not returned any Community Group.
    if (count(json_decode($response_committee)) == 0) {
      drupal_set_message('You do not have any Community Group to select',
        'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  /*public function validateForm(array &$form, FormStateInterface $form_state) {

  }*/

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get posted values and assign to an array. This array is using same ids
    // as in the API and it will be converted to JSON format for further
    // posting to the API.
    $send_email_data = [
      'ContactId' => Xss::filter($form_state->getValue('contact_id')),
      'CommunityGroupID' => Xss::filter($form_state->getValue('community_group')),
      'Subject' => Xss::filter($form_state->getValue('email_subject')),
      'EmailBody' => nl2br(Xss::filter($form_state->getValue('email_message'))),
    ];

    // Get subscription key.
    // @todo - Function definition is wrong and it should have correct module
    //   name as prefix i.e bifm_api_interface_.
    $subscription_key = bifm_get_subscription_key();

    // Get environment.
    // @todo - Function definition is wrong and it should have correct module
    //   name as prefix i.e bifm_api_interface_.
    $environment = bifm_get_environment();

    // Get config data.
    $config = \Drupal::config('bifm.api_interface');

    // Get API URL for Committee Send Email API.
    $url = $config->get($environment . '_send_email_path');

    // Create a new cURL resource.
    $ch = curl_init($url);

    // Attach encoded JSON string to the POST fields.
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($send_email_data));

    // Set headers.
    $request_headers_rc = [];
    $request_headers_rc[] = "Content-Type: application/json";
    $request_headers_rc[] = "Ocp-Apim-Subscription-Key: $subscription_key";
    $result_rc = [];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers_rc);

    // Return response instead of outputting.
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    // Execute the POST request.
    $result = curl_exec($ch);

    // Close cURL resource.
    curl_close($ch);

    // Display success message.
    if (json_decode($result)->unableToSendTo == '') {
      drupal_set_message($this->t('Emails successfully sent.'));
    }

    // Display error message.
    if (json_decode($result)->unableToSendTo) {
      drupal_set_message($this->t('Could not send email(s) to :emails',
        [':emails' => json_decode($result)->unableToSendTo]), 'error');
    }

    // Load current user object.
    $user = User::load(\Drupal::currentUser()->id());

    // Redirect path.
    // Update http:// to https:// as per server settings.
    $redirect_path = trim('http://' . \Drupal::request()->getHost() . '/user/' .
    $user->get('uid')->value);

    // Redirect to member dashboard.
    $response = new TrustedRedirectResponse(
      Url::fromUri($redirect_path)->toString());
    $form_state->setResponse($response);
  }

}
